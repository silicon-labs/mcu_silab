
USING: accessors arrays kernel models tools.continuations ;

IN: mcu-silab.siutil

TUPLE: siutil array ;

 SYMBOL: dialog



! int Connect(undefined4 param_1,int param_2,int param_3,int param_4)
! param_4 = bitrate
! param_3 = proto
! param_2 = dialog
! param_1 = com
: connect ( bitrate proto dialog com -- HRESULT )

  [ dup ] dip swap 0 =
  [ 0 dialog set ]
  [
    swap 1 = not
    [  ] when
    1 diaglog set
  ] if


    FUN_1001f720(2);
    if ((*(int *)(DAT_101e2308 + 0x78) != 0) && (iVar2 = Disconnect(param_1), iVar2 != 0))
    {
        ! FUN_10034ebd(); display error message
      return 0x81000009;
    }
    if ((5 < param_4) || (param_4 < 0))
    {
      param_4 = 0;
    }
    uVar3 = BaudRateLookup(param_4);
    FUN_100062a0(uVar3);
    if ((*(int *)(DAT_101e2308 + 0x90) == 3) || (iVar2 = FUN_10015520(param_1), iVar2 != 0))
    {
      if ((param_3 == 0) || (param_3 == 1))
      {
        *(undefined *)(DAT_101e2308 + 0x6d) = (char)param_3;
        iVar2 = FUN_100078a0();
        if ((iVar2 < 0) || (*(int *)(DAT_101e2308 + 0x78) == 0))
        {
          *(undefined4 *)(DAT_101e2308 + 0x78) = 1;
          FUN_10006930(0,0,0);
        }
        FUN_10034ebd();
        return iVar2;
      }
    }
    else
    {
      FUN_1001d480("Could not set COM port. Check COM port settings.",0x30);
    }
    FUN_10034ebd();
    return 0x80070057;
  ;

: connected ( -- ? )
  ;

: connect-usb ( dialog power poto sn -- hresult )
;

: disconnect ( com -- hresult )
  ;

: disconnect-usb ( -- hresult )
  ;


: download  ( pf lf bs sp dialog erase filename -- hresult )
;

: verify-flash-download ( bs dialog filename -- hresult )
;


: flash-erase ( proto dialog com -- hresult )
;


: flash-erase-usb ( proto dialog sn -- hresult )
;


: get-codememory ( len start mem -- hresult )
;


: get-devicename ( -- name )
;

: get-dllversion ( -- version )
;

: get-errormsg ( -- error )
;


: get-ram-memory ( len start ptrMem -- hresult )
;


: get-firmware-version ( -- version )
;

: get-scratchpad-memory ( len start mem -- hresult )
;

: get-usb-device ( sn -- hresult )
;

: get-usb-device-sn ( sn devnum -- hresult )
;

: get-usb-dll-version ( -- version )
;

: get-usb-firmware-version ( -- version )
;

: get-xram-memory ( len start mem -- hresult )
;

: support-banking ( -- ? )
  ;

: set-adapter ( adapter -- hresult )
;

: set-appname ( name -- )
;

: set-code-memory ( dialog len start mem -- hresult )
;

: set-jtag-device ( bafter bbefore dafter dbefore dialog com -- hresult )
;

: set-jtag-device-usb ( bafter bbefore dafter dbefore dialog power sn -- hresult )
;

: set-ram-memory ( len start mem -- hresult )
;

: set-scratpad-memory ( dialog len start mem -- hresult )
;

: set-target-go ( -- )
;

: set-target-halt ( -- )
;

: set-usb-device ( sn -- hresult )
;

: set-xram-memory ( len start mem -- hresult )
;

: target-reset ( -- )
;

! List all EC and ToolStick debug adapters found
: usb-debug-device ( -- devices )

;
