
int FLASHEraseUSB(undefined4 param_1,int param_2,int param_3)

{
  AFX_MODULE_STATE *pAVar1;
  int iVar2;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x25ab0  36  FLASHEraseUSB */
  pAVar1 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar1);
  if (param_2 == 0) {
    *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 0;
  }
  else {
    if (param_2 != 1) {
      FUN_10034ebd();
      return 0x80070057;
    }
    *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 1;
  }
  FUN_1001f720(3);
  if ((param_3 != 0) && (param_3 != 1)) {
    FUN_10034ebd();
    return 0x80070057;
  }
  iVar2 = FUN_10029060(param_1);
  if (-1 < iVar2) {
    if (*(int *)(DAT_101e2308 + 0x78) != 0) {
      FUN_10034ebd();
      return 0x8100000d;
    }
    *(undefined *)(DAT_101e2308 + 0x6d) = (char)param_3;
    iVar2 = FUN_10015d50();
  }
  FUN_10034ebd();
  return iVar2;
}

