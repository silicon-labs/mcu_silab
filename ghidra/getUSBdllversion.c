
undefined4 GetUSBDLLVersion(undefined4 param_1)

{
  AFX_MODULE_STATE *pAVar1;
  undefined4 uVar2;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x25da0  39  GetUSBDLLVersion */
  pAVar1 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar1);
  uVar2 = GetHIDDLLVersion(param_1);
  FUN_10034ebd();
  return uVar2;
}

