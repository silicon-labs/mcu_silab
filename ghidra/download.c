
int Download(undefined4 param_1,uint param_2,int param_3,int param_4,int param_5,int param_6,
            int param_7)

{
  char cVar1;
  char cVar2;
  AFX_MODULE_STATE *pAVar3;
  int iVar4;
  undefined4 uVar5;
  uint uVar6;
  byte bVar7;
  undefined4 uVar8;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x24e00  2  Download */
  pAVar3 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar3);
  if (param_3 == 0) {
    *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 0;
  }
  else {
    if (param_3 != 1) goto LAB_10025065;
    *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 1;
  }
  if ((param_2 != 0) && (param_2 != 1)) {
LAB_10025065:
    FUN_10034ebd();
    return 0x80070057;
  }
  cVar1 = (char)param_2;
  if (param_4 == 0) {
    bVar7 = 0;
  }
  else {
    if (param_4 != 1) goto LAB_10025051;
    bVar7 = 1;
  }
  param_2 = param_2 & 0xffffff00 | (uint)bVar7;
  if (param_5 == 0) {
LAB_10025051:
    FUN_10034ebd();
    return 0x80070057;
  }
  if (*(int *)(DAT_101e2308 + 0x78) == 0) {
    FUN_10034ebd();
    return 0x8100801b;
  }
  cVar2 = FUN_10027980(param_1);
  if (cVar2 == '\0') {
    FUN_1001d480("File not found.\r\nDownload aborted.",0x30);
    FUN_10034ebd();
    return 0x81000004;
  }
  if (cVar1 != '\0') {
    if ((bVar7 == 0) && (iVar4 = FUN_10015cf0(1), iVar4 < 0)) {
      FUN_1001d5c0("User Code Erase Error.\r\nDownload aborted.");
      *(undefined4 *)(DAT_101e2308 + 0x78) = 1;
      FUN_10006930(0,0,0);
      FUN_10034ebd();
      return 0x81000008;
    }
    iVar4 = FUN_10006770(0,1,1);
    if (iVar4 < 0) {
      FUN_1001d480("Target could not be reset.\r\nConfirm cable and power connections and retry.",
                   0x30);
      goto LAB_10024f56;
    }
  }
  uVar6 = (uint)(param_7 != 0);
  uVar8 = *(undefined4 *)(DAT_101e2308 + 0x2a6c);
  uVar5 = FUN_10006490(param_2,param_5,uVar8,uVar6);
  iVar4 = FUN_100274c0(param_1,&DAT_101482b1,uVar5,param_2,param_5,uVar8,uVar6);
  if (iVar4 == -0x7effffef) {
    FUN_10034ebd();
    return 0x8100801b;
  }
  if (*(char *)(DAT_101e2308 + 0x15e) == '\0') {
    if (-1 < iVar4) {
      FUN_1001d5c0("Download Error.\r\nDownload aborted.");
    }
    *(undefined4 *)(DAT_101e2308 + 0x78) = 1;
    FUN_10006930(0,0,0);
    FUN_10034ebd();
    if (iVar4 < 0) {
      return iVar4;
    }
    return 0x81000006;
  }
  if ((param_6 != 0) && (iVar4 = FUN_10026510(0,0), iVar4 < 0)) {
    FUN_10034ebd();
    return 0x81000002;
  }
LAB_10024f56:
  FUN_10034ebd();
  return iVar4;
}

