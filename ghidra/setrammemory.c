
undefined4 SetRAMMemory(int param_1,int param_2,int param_3)

{
  uint *puVar1;
  AFX_MODULE_STATE *pAVar2;
  uint uVar3;
  undefined4 uVar4;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x252a0  11  SetRAMMemory */
  pAVar2 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar2);
  if ((param_1 != 0) && (param_3 != 0)) {
    uVar3 = param_3 + param_2;
    puVar1 = (uint *)(*(int *)(DAT_101e2308 + 0x58) + 0x14);
    if (uVar3 < *puVar1 || uVar3 == *puVar1) {
      if (*(int *)(DAT_101e2308 + 0x78) == 0) {
        FUN_10034ebd();
        return 0x8100801b;
      }
      uVar4 = FUN_10027040(0,param_2,uVar3 - 1,param_1);
      FUN_10034ebd();
      return uVar4;
    }
  }
  FUN_10034ebd();
  return 0x80070057;
}

