
undefined4 GetUSBDeviceSN(undefined4 param_1,undefined4 param_2)

{
  AFX_MODULE_STATE *pAVar1;
  undefined4 uVar2;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x25d60  33  GetUSBDeviceSN */
  pAVar1 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar1);
  uVar2 = GetDeviceSerialNbr(param_1,param_2);
  FUN_10034ebd();
  return uVar2;
}

