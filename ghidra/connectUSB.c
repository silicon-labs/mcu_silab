
int ConnectUSB(char *param_1,int param_2,undefined4 param_3,int param_4)

{
  AFX_MODULE_STATE *pAVar1;
  int iVar2;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x26210  30  ConnectUSB */
  pAVar1 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar1);
  if (param_4 == 0) {
    *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 0;
  }
  else {
    if (param_4 != 1) {
      FUN_10034ebd();
      return 0x80070057;
    }
    *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 1;
  }
  if (*(int *)(DAT_101e2308 + 0x8c) == 0) {
    FUN_1001f720(3);
    if ((*(int *)(DAT_101e2308 + 0x78) != 0) && (iVar2 = Disconnect(1), iVar2 != 0)) {
      FUN_10034ebd();
      return 0x8100000a;
    }
    if ((param_2 != 0) && (param_2 != 1)) {
      FUN_10034ebd();
      return 0x80070057;
    }
    *(undefined *)(DAT_101e2308 + 0x6d) = (char)param_2;
    FUN_10028dc0(param_3);
    iVar2 = __stricmp(param_1,"");
    if ((iVar2 != 0) && (iVar2 = FUN_10028f80(param_1), iVar2 < 0)) goto LAB_1002630a;
  }
  iVar2 = FUN_100078a0();
  if ((-1 < iVar2) && (*(int *)(DAT_101e2308 + 0x78) == 0)) {
    FUN_10034ebd();
    return 0x8100801b;
  }
LAB_1002630a:
  FUN_10034ebd();
  return iVar2;
}

