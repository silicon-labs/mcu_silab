
undefined4 SetXRAMMemory(int param_1,int param_2,int param_3)

{
  AFX_MODULE_STATE *pAVar1;
  undefined4 uVar2;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x253d0  10  SetXRAMMemory */
  pAVar1 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar1);
  if ((param_1 == 0) || (param_3 == 0)) {
    FUN_10034ebd();
    return 0x80070057;
  }
  if (*(int *)(DAT_101e2308 + 0x78) == 0) {
    FUN_10034ebd();
    return 0x8100801b;
  }
  if (*(int *)(*(int *)(DAT_101e2308 + 0x58) + 0x34) == 0) {
    FUN_10034ebd();
    return 0x8100000c;
  }
  uVar2 = FUN_10027040(2,param_2,param_2 + -1 + param_3,param_1);
  FUN_10034ebd();
  return uVar2;
}

