
int TargetReset(void)

{
  AFX_MODULE_STATE *pAVar1;
  int iVar2;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x25980  28  TargetReset */
  pAVar1 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar1);
  if (*(int *)(DAT_101e2308 + 0x78) == 0) {
    FUN_10034ebd();
    return 0x8100801b;
  }
  iVar2 = FUN_10006770(0,1,1);
  if (-1 < iVar2) {
    FUN_10006020();
  }
  FUN_10034ebd();
  return iVar2;
}

