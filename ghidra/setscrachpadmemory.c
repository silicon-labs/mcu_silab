
undefined4 SetScratchPadMemory(int param_1,undefined4 param_2,int param_3,int param_4)

{
  AFX_MODULE_STATE *pAVar1;
  undefined4 uVar2;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x257f0  45  SetScratchPadMemory */
  pAVar1 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar1);
  if (param_1 == 0) {
    FUN_10034ebd();
    return 0x80070057;
  }
  if (param_3 != 0) {
    if (param_4 == 0) {
      *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 0;
    }
    else {
      if (param_4 != 1) goto LAB_100258a1;
      *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 1;
    }
    if (*(int *)(DAT_101e2308 + 0x78) == 0) {
      FUN_10034ebd();
      return 0x8100801b;
    }
    FUN_100221f0(param_3,param_2,param_1);
    uVar2 = FUN_10022910(param_3,param_2,param_1);
    FUN_10034ebd();
    return uVar2;
  }
LAB_100258a1:
  FUN_10034ebd();
  return 0x80070057;
}

