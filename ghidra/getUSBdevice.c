
undefined4 GetUSBDevice(undefined4 param_1)

{
  AFX_MODULE_STATE *pAVar1;
  undefined4 uVar2;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x25de0  34  GetUSBDevice */
  pAVar1 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar1);
  uVar2 = GetSelectedDeviceSerialNbr(param_1);
  FUN_10034ebd();
  return uVar2;
}

