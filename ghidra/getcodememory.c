
undefined4 GetCodeMemory(int param_1,uint param_2,int param_3)

{
  uint *puVar1;
  uint uVar2;
  int iVar3;
  AFX_MODULE_STATE *pAVar4;
  undefined4 uVar5;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x25470  8  GetCodeMemory */
  pAVar4 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar4);
  if (param_1 == 0) {
    FUN_10034ebd();
    return 0x80070057;
  }
  if (param_3 == 0) {
    FUN_10034ebd();
    return 0x80070057;
  }
  if (*(int *)(DAT_101e2308 + 0x78) == 0) {
    FUN_10034ebd();
    return 0x8100801b;
  }
  if (((*(int *)(DAT_101e2308 + 0x2a8c) == 0) &&
      (*(uint *)(*(int *)(DAT_101e2308 + 0x58) + 0x20) <= param_2)) &&
     (puVar1 = (uint *)(*(int *)(DAT_101e2308 + 0x58) + 0x1c),
     param_2 < *puVar1 || param_2 == *puVar1)) {
    FUN_10034ebd();
    return 0x80070057;
  }
  iVar3 = *(int *)(DAT_101e2308 + 0x58);
  uVar2 = (param_2 - 1) + param_3;
  if ((*(uint *)(iVar3 + 0x20) < uVar2) && (uVar2 < *(uint *)(iVar3 + 0x1c))) {
    FUN_10034ebd();
    return 0x80070057;
  }
  if (((*(uint *)(iVar3 + 0x20) <= uVar2) && (*(uint *)(iVar3 + 0x1c) <= uVar2)) &&
     (*(uint *)(iVar3 + 0x1c) == 0xffff)) {
    FUN_10003b10(0x80);
    uVar5 = FUN_1001aaa0(param_3,param_2,param_1,1,1,0);
    FUN_10003b10(0);
    FUN_10034ebd();
    return uVar5;
  }
  if ((param_2 == *(uint *)(iVar3 + 0x2c)) || (param_2 == *(uint *)(iVar3 + 0x18))) {
    uVar5 = FUN_1001aee0(param_2,param_1,1);
  }
  else {
    uVar5 = FUN_10026750(param_1,param_2,param_3,1);
  }
  FUN_10034ebd();
  return uVar5;
}

