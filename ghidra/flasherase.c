
undefined4 FLASHErase(undefined4 param_1,int param_2,int param_3)

{
  AFX_MODULE_STATE *pAVar1;
  int iVar2;
  undefined4 uVar3;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x259e0  15  FLASHErase */
  pAVar1 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar1);
  if (param_2 == 0) {
    *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 0;
  }
  else {
    if (param_2 != 1) goto LAB_10025a7a;
    *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 1;
  }
  FUN_1001f720(2);
  if ((param_3 == 0) || (param_3 == 1)) {
    *(undefined *)(DAT_101e2308 + 0x6d) = (char)param_3;
    if (*(int *)(DAT_101e2308 + 0x78) != 0) {
      FUN_10034ebd();
      return 0x8100000d;
    }
    iVar2 = FUN_10015520(param_1);
    if (iVar2 != 0) {
      uVar3 = FUN_10015d50();
      FUN_10034ebd();
      return uVar3;
    }
    FUN_1001d480("Could not set COM port. Check COM port settings.",0x30);
  }
LAB_10025a7a:
  FUN_10034ebd();
  return 0x80070057;
}

