
int Connect(undefined4 param_1,int param_2,int param_3,int param_4)

{
  AFX_MODULE_STATE *pAVar1;
  int iVar2;
  undefined4 uVar3;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x260c0  1  Connect */
  pAVar1 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar1);
  if (param_2 == 0) {
    *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 0;
  }
  else {
    if (param_2 != 1) {
      FUN_10034ebd();
      return 0x80070057;
    }
    *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 1;
  }
  FUN_1001f720(2);
  if ((*(int *)(DAT_101e2308 + 0x78) != 0) && (iVar2 = Disconnect(param_1), iVar2 != 0)) {
    FUN_10034ebd();
    return 0x81000009;
  }
  if ((5 < param_4) || (param_4 < 0)) {
    param_4 = 0;
  }
  uVar3 = BaudRateLookup(param_4);
  FUN_100062a0(uVar3);
  if ((*(int *)(DAT_101e2308 + 0x90) == 3) || (iVar2 = FUN_10015520(param_1), iVar2 != 0)) {
    if ((param_3 == 0) || (param_3 == 1)) {
      *(undefined *)(DAT_101e2308 + 0x6d) = (char)param_3;
      iVar2 = FUN_100078a0();
      if ((iVar2 < 0) || (*(int *)(DAT_101e2308 + 0x78) == 0)) {
        *(undefined4 *)(DAT_101e2308 + 0x78) = 1;
        FUN_10006930(0,0,0);
      }
      FUN_10034ebd();
      return iVar2;
    }
  }
  else {
    FUN_1001d480("Could not set COM port. Check COM port settings.",0x30);
  }
  FUN_10034ebd();
  return 0x80070057;
}

