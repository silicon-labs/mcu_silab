
undefined4 SetCodeMemory(byte *param_1,uint param_2,uint param_3,int param_4)

{
  uint uVar1;
  byte *pbVar2;
  byte bVar3;
  AFX_MODULE_STATE *pAVar4;
  int iVar5;
  undefined4 uVar6;
  AFX_MAINTAIN_STATE2 local_c [8];
  
  iVar5 = param_3;
                    /* 0x25610  9  SetCodeMemory */
  pbVar2 = param_1;
  pAVar4 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar4);
  if (param_1 == (byte *)0x0) {
    FUN_10034ebd();
    return 0x80070057;
  }
  if (param_3 == 0) {
LAB_100257ce:
    FUN_10034ebd();
    return 0x80070057;
  }
  if (param_4 == 0) {
    *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 0;
  }
  else {
    if (param_4 != 1) goto LAB_100257ce;
    *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 1;
  }
  if (*(int *)(DAT_101e2308 + 0x78) == 0) {
    FUN_10034ebd();
    return 0x8100801b;
  }
  if (((param_3 != 1) && (param_3 != 2)) ||
     ((param_2 != *(uint *)(*(int *)(DAT_101e2308 + 0x58) + 0x2c) &&
      (param_2 != *(uint *)(*(int *)(DAT_101e2308 + 0x58) + 0x18))))) {
    iVar5 = FUN_10015f40(param_2,param_3);
    if (iVar5 == 1) {
      FUN_10034ebd();
      return 0x81000001;
    }
    iVar5 = *(int *)(DAT_101e2308 + 0x58);
    if (((param_2 < *(uint *)(iVar5 + 0x20)) ||
        (*(uint *)(iVar5 + 0x1c) <= param_2 && param_2 != *(uint *)(iVar5 + 0x1c))) &&
       ((uVar1 = (param_2 - 1) + param_3, uVar1 <= *(uint *)(iVar5 + 0x20) ||
        (*(uint *)(iVar5 + 0x1c) <= uVar1)))) {
      if ((*(uint *)(iVar5 + 0x1c) <= uVar1) && (*(uint *)(iVar5 + 0x1c) == 0xffff)) {
        FUN_100221f0(param_3,param_2,param_1);
        FUN_10022910(param_3,param_2,param_1);
        FUN_10034ebd();
        return 0;
      }
      uVar6 = FUN_10027040(1,param_2,uVar1,param_1);
      FUN_10034ebd();
      return uVar6;
    }
    FUN_10034ebd();
    return 0x80070057;
  }
  param_3 = CONCAT31(param_3._1_3_,0xff);
  param_1 = (byte *)CONCAT31(param_1._1_3_,0xff);
  if (iVar5 == 2) {
    bVar3 = *pbVar2;
    param_3 = (uint)pbVar2[1];
  }
  else {
    if (iVar5 != 1) goto LAB_10025799;
    bVar3 = *pbVar2;
  }
  param_1 = (byte *)((uint)pbVar2 & 0xffffff00 | (uint)bVar3);
LAB_10025799:
  iVar5 = FUN_10026510(param_3,param_1);
  if (-1 < iVar5) {
    FUN_10034ebd();
    return 0;
  }
  FUN_10034ebd();
  return 0x81000002;
}

