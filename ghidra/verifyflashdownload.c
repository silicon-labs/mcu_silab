
int VerifyFlashDownload(undefined4 param_1,int param_2,undefined4 param_3)

{
  char cVar1;
  AFX_MODULE_STATE *pAVar2;
  int iVar3;
  int *piVar4;
  undefined4 uVar5;
  char *pcVar6;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x25f40  46  VerifyFlashDownload */
  pAVar2 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar2);
  if (param_2 == 0) {
    *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 0;
  }
  else {
    if (param_2 != 1) {
      FUN_10034ebd();
      return 0x80070057;
    }
    *(undefined4 *)(DAT_101e2308 + 0x2a6c) = 1;
  }
  if (*(int *)(DAT_101e2308 + 0x78) == 0) {
    FUN_10034ebd();
    return 0x8100801b;
  }
  cVar1 = FUN_10027980(param_1);
  if (cVar1 == '\0') {
    pcVar6 = "File not found.\r\nDownload aborted.";
  }
  else {
    iVar3 = FUN_10026500(param_1);
    if (iVar3 != 0) {
      FUN_10016370();
      piVar4 = (int *)FUN_10032679();
      if (piVar4 == (int *)0x0) {
        uVar5 = 0;
      }
      else {
        uVar5 = (**(code **)(*piVar4 + 0x74))();
      }
      iVar3 = FUN_100277b0(param_1,uVar5,param_3,*(undefined4 *)(DAT_101e2308 + 0x2a6c));
      FUN_10034ebd();
      if (iVar3 == -0x7effffef) {
        return 0x8100801b;
      }
      return iVar3;
    }
    pcVar6 = "File entered is not a Hex file.\r\nDownload aborted.";
  }
  FUN_1001d480(pcVar6,0x30);
  FUN_10034ebd();
  return 0x81000004;
}

