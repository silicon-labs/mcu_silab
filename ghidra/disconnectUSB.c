
undefined4 DisconnectUSB(void)

{
  AFX_MODULE_STATE *pAVar1;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x251c0  31  DisconnectUSB */
  pAVar1 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar1);
  if (*(int *)(DAT_101e2308 + 0x78) == 0) {
    FUN_10034ebd();
    return 0x8100801b;
  }
  FUN_1001e610();
  FUN_10034ebd();
  return 0;
}

