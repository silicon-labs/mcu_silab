
int DownloadCS(undefined4 param_1,undefined4 param_2,undefined4 param_3,undefined4 param_4,
              undefined4 param_5,undefined4 param_6,undefined4 param_7,undefined2 *param_8)

{
  AFX_MODULE_STATE *pAVar1;
  int iVar2;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x25080  47  DownloadCS */
  pAVar1 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar1);
  iVar2 = Download(param_1,param_2,param_3,param_4,param_5,param_6,param_7);
  if ((-1 < iVar2) && (param_8 != (undefined2 *)0x0)) {
    *param_8 = *(undefined2 *)(DAT_101e2308 + 0x2a92);
  }
  FUN_10034ebd();
  return iVar2;
}

