
undefined4 ISupportBanking(undefined4 *param_1)

{
  short sVar1;
  AFX_MODULE_STATE *pAVar2;
  int iVar3;
  undefined4 uVar4;
  AFX_MAINTAIN_STATE2 local_c [8];
  
                    /* 0x25b90  18  ISupportBanking */
  pAVar2 = AfxGetModuleState();
  AFX_MAINTAIN_STATE2(local_c,pAVar2);
  if (param_1 == (undefined4 *)0x0) {
    FUN_10034ebd();
    return 0x80070057;
  }
  iVar3 = Connected();
  if (iVar3 == 0) {
    FUN_10034ebd();
    return 0x8100801b;
  }
  sVar1 = *(short *)(*(int *)(DAT_101e2308 + 0x58) + 0x30);
  uVar4 = 0;
  if (((sVar1 == 7) || (sVar1 == 0x20)) || (sVar1 == 0x2a)) {
    uVar4 = 3;
  }
  *param_1 = uVar4;
  FUN_10034ebd();
  return 0;
}

