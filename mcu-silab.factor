

USING: accessors arrays kernel help.topics models mcu-silab.part-list
        tools.continuations
        ui.gadgets ui.gadgets.borders ui.gadgets.menus
        ui.gadgets.status-bar ui.gadgets.worlds
        ui.gadgets.tracks ui.gestures ui.commands ui.gadgets.labels
        ui.tools.common ui.tools.browser
        ui.tools.browser.history ;

IN: mcu-silab


TUPLE: silab-gadget < tool history search-field scroller ;


: <mcu-silab> ( -- )
  part-list drop ;


silab-gadget "toolbar" f {
  { T{ key-down f { A+ } "LEFT" } com-back }
  { T{ key-down f { A+ } "RIGHT" } com-forward }
} define-command-map


: <mcu-gadget> ( link -- gadget )
    vertical silab-gadget new-track with-lines 1 >>fill
    swap >link <model> >>model
    "m1" <label> "m2" <label> 2array <menu> add-gadget
    ! dup <history> >>history
    ! dup <search-field> >>search-field
    ! add-browser-toolbar
    "label" <label> { 0 0 } <filled-border> add-gadget
    ! add-gadget
    ! add-help-header add-help-pane
    ! add-help-footer
     ;

: (mcu-window) ( topic -- )
  break
  <mcu-gadget> <world-attributes> "MCU" >>title
  open-status-window ;

: mcu-window ( -- )
  "Si-LAB" (mcu-window) ;
