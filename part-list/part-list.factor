
USING: kernel sequences ;

IN: mcu-silab.part-list

TUPLE: mcu-device part-number wlock-addr rlock-addr num-banks ;

: <mcu-device> ( part wlock rlock banks -- mcu-devce )
  mcu-device boa ;

: spush ( v m -- v )
  swap [ push ] keep ;

: part-list ( -- list )
  V{ } clone
  0 0x07DFF 0x07DFE "C8051F000" <mcu-device> spush
  0 0x07DFF 0x07DFE "C8051F001" <mcu-device> spush
  0 0x07DFF 0x07DFE "C8051F002" <mcu-device> spush
  0 0x07DFF 0x07DFE "C8051F005" <mcu-device> spush
  0 0x07DFF 0x07DFE "C8051F006" <mcu-device> spush
  0 0x07DFF 0x07DFE "C8051F007" <mcu-device> spush
  0 0x07DFF 0x07DFE "C8051F010" <mcu-device> spush
  0 0x07DFF 0x07DFE "C8051F011" <mcu-device> spush
  0 0x07DFF 0x07DFE "C8051F012" <mcu-device> spush
  0 0x07DFF 0x07DFE "C8051F015" <mcu-device> spush
  0 0x07DFF 0x07DFE "C8051F016" <mcu-device> spush
  0 0x07DFF 0x07DFE "C8051F017" <mcu-device> spush
! 16K
  0 0x03DFF 0x03DFE "C8051F018" <mcu-device> spush
  0 0x03DFF 0x03DFE "C8051F019" <mcu-device> spush
! F02x  64K
  0 0x0FDFF 0x0FDFE "C8051F020" <mcu-device> spush
  0 0x0FDFF 0x0FDFE "C8051F021" <mcu-device> spush
  0 0x0FDFF 0x0FDFE "C8051F022" <mcu-device> spush
  0 0x0FDFF 0x0FDFE "C8051F023" <mcu-device> spush
! F04x 64K
  0 0x0FDFF 0x0FDFE "C8051F040" <mcu-device> spush
  0 0x0FDFF 0x0FDFE "C8051F041" <mcu-device> spush
  0 0x0FDFF 0x0FDFE "C8051F042" <mcu-device> spush
  0 0x0FDFF 0x0FDFE "C8051F043" <mcu-device> spush
  0 0x0FDFF 0x0FDFE "C8051F044" <mcu-device> spush
  0 0x0FDFF 0x0FDFE "C8051F045" <mcu-device> spush
! 32K
  0 0x07FFF 0x07FFE "C8051F046" <mcu-device> spush
  0 0x07FFF 0x07FFE "C8051F047" <mcu-device> spush
! F06x 64K
  0 0x0FBFF 0x0FBFE "C8051F060" <mcu-device> spush
  0 0x0FBFF 0x0FBFE "C8051F061" <mcu-device> spush
  0 0x0FBFF 0x0FBFE "C8051F062" <mcu-device> spush
  0 0x0FBFF 0x0FBFE "C8051F063" <mcu-device> spush
  0 0x0FBFF 0x0FBFE "C8051F064" <mcu-device> spush
  0 0x0FBFF 0x0FBFE "C8051F065" <mcu-device> spush
! 32K
  0 0x07FFF 0x07FFE "C8051F066" <mcu-device> spush
  0 0x07FFF 0x07FFE "C8051F067" <mcu-device> spush
! F12x F13x 128K
  3 0x1FBFF 0x1FBFE "C8051F120" <mcu-device> spush
  3 0x1FBFF 0x1FBFE "C8051F121" <mcu-device> spush
  3 0x1FBFF 0x1FBFE "C8051F122" <mcu-device> spush
  3 0x1FBFF 0x1FBFE "C8051F123" <mcu-device> spush
  3 0x1FBFF 0x1FBFE "C8051F124" <mcu-device> spush
  3 0x1FBFF 0x1FBFE "C8051F125" <mcu-device> spush
  3 0x1FBFF 0x1FBFE "C8051F126" <mcu-device> spush
  3 0x1FBFF 0x1FBFE "C8051F127" <mcu-device> spush
  3 0x1FBFF 0x1FBFE "C8051F130" <mcu-device> spush
  3 0x1FBFF 0x1FBFE "C8051F131" <mcu-device> spush
! 64K
  3 0x0FFFF 0x0FFFE "C8051F132" <mcu-device> spush
  3 0x0FFFF 0x0FFFE "C8051F133" <mcu-device> spush
! F2xx 8K
  0 0x01DFF 0x01DFE "C8051F206" <mcu-device> spush
  0 0x01DFF 0x01DFE "C8051F220" <mcu-device> spush
  0 0x01DFF 0x01DFE "C8051F221" <mcu-device> spush
  0 0x01DFF 0x01DFE "C8051F226" <mcu-device> spush
  0 0x01DFF 0x01DFE "C8051F230" <mcu-device> spush
  0 0x01DFF 0x01DFE "C8051F231" <mcu-device> spush
  0 0x01DFF 0x01DFE "C8051F236" <mcu-device> spush
! F30x 8K
  0 0x01DFF 0x01DFF "C8051F300" <mcu-device> spush
  0 0x01DFF 0x01DFF "C8051F301" <mcu-device> spush
  0 0x01DFF 0x01DFF "C8051F302" <mcu-device> spush
  0 0x01DFF 0x01DFF "C8051F303" <mcu-device> spush
! 4K
  0 0x00FFF 0x00FFF "C8051F304" <mcu-device> spush
! 2K
  0 0x007FF 0x007FF "C8051F305" <mcu-device> spush
! F31x 16K
  0 0x03DFF 0x03DFF "C8051F310" <mcu-device> spush
  0 0x03DFF 0x03DFF "C8051F311" <mcu-device> spush
! 8K
  0 0x01FFF 0x01FFF "C8051F312" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F313" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F314" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F315" <mcu-device> spush
! 16K
  0 0x03DFF 0x03DFF "C8051F316" <mcu-device> spush
  0 0x03DFF 0x03DFF "C8051F317" <mcu-device> spush
! F320-1 16K
  0 0x03DFF 0x03DFF "C8051F320" <mcu-device> spush
  0 0x03DFF 0x03DFF "C8051F321" <mcu-device> spush
! F326-7 16K
  0 0x03DFF 0x03DFF "C8051F326" <mcu-device> spush
  0 0x03DFF 0x03DFF "C8051F327" <mcu-device> spush
! F33x 8K
  0 0x01DFF 0x01DFF "C8051F330" <mcu-device> spush
  0 0x01DFF 0x01DFF "C8051F331" <mcu-device> spush
! 4K
  0 0x00FFF 0x00FFF "C8051F332" <mcu-device> spush
  0 0x00FFF 0x00FFF "C8051F333" <mcu-device> spush
! 2K
  0 0x007FF 0x007FF "C8051F334" <mcu-device> spush
  0 0x007FF 0x007FF "C8051F335" <mcu-device> spush
! 16K
  0 0x03DFF 0x03DFF "C8051F336" <mcu-device> spush
  0 0x03DFF 0x03DFF "C8051F337" <mcu-device> spush
  0 0x03DFF 0x03DFF "C8051F338" <mcu-device> spush
  0 0x03DFF 0x03DFF "C8051F339" <mcu-device> spush
! F34x 64K
  0 0x0FBFF 0x0FBFF "C8051F340" <mcu-device> spush
! 32K
  0 0x07FFF 0x07FFF "C8051F341" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F342" <mcu-device> spush
  0 0x07FFF 0x07FFF "C8051F343" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F344" <mcu-device> spush
  0 0x07FFF 0x07FFF "C8051F345" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F346" <mcu-device> spush
  0 0x07FFF 0x07FFF "C8051F347" <mcu-device> spush
  0 0x0fBFF 0x0FBFF "C8051F348" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F349" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F34A" <mcu-device> spush
  0 0x070FF 0x07FFF "C8051F34B" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F34C" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F34D" <mcu-device> spush
! F35x 8K
  0 0x01DFF 0x01DFF "C8051F350" <mcu-device> spush
  0 0x01DFF 0x01DFF "C8051F351" <mcu-device> spush
  0 0x01DFF 0x01DFF "C8051F352" <mcu-device> spush
  0 0x01DFF 0x01DFF "C8051F353" <mcu-device> spush
! F36x 32K
  0 0x07BFF 0x07BFF "C8051F360" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F361" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F362" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F363" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F364" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F365" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F366" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F367" <mcu-device> spush
! 16K
  0 0x03FFF 0x03FFF "C8051F368" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F369" <mcu-device> spush
! F38x
  0 0x0FBFF 0x0FBFF "C8051F380" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F381" <mcu-device> spush
  0 0x07FFF 0x07FFF "C8051F382" <mcu-device> spush
  0 0x07FFF 0x07FFF "C8051F383" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F384" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F385" <mcu-device> spush
  0 0x07FFF 0x07FFF "C8051F386" <mcu-device> spush
  0 0x07FFF 0x07FFF "C8051F387" <mcu-device> spush
! F41x 32K
  0 0x07DFF 0x07DFF "C8051F410" <mcu-device> spush
  0 0x07DFF 0x07DFF "C8051F411" <mcu-device> spush
! 16K
  0 0x03FFF 0x03FFF "C8051F412" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F413" <mcu-device> spush
! F50x
  0 0x0FBFF 0x0FBFF "C8051F500" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F501" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F502" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F503" <mcu-device> spush
  0 0x07FFF 0x07FFF "C8051F504" <mcu-device> spush
  0 0x07FFF 0x07FFF "C8051F505" <mcu-device> spush
  0 0x07FFF 0x07FFF "C8051F506" <mcu-device> spush
  0 0x07FFF 0x07FFF "C8051F507" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F508" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F509" <mcu-device> spush
  0 0x07FFF 0x07FFF "C8051F510" <mcu-device> spush
  0 0x07FFF 0x07FFF "C8051F511" <mcu-device> spush
! F53x/52x 8K
  0 0x01DFF 0x01DFF "C8051F520" <mcu-device> spush
  0 0x01DFF 0x01DFF "C8051F521" <mcu-device> spush
! 4K
  0 0x00FFF 0x00FFF "C8051F523" <mcu-device> spush
  0 0x00FFF 0x00FFF "C8051F524" <mcu-device> spush
! 2K
  0 0x007FF 0x007FF "C8051F526" <mcu-device> spush
  0 0x007FF 0x007FF "C8051F527" <mcu-device> spush
! 8K
  0 0x01DFF 0x01DFF "C8051F530" <mcu-device> spush
  0 0x01DFF 0x01DFF "C8051F531" <mcu-device> spush
! 4K
  0 0x00FFF 0x00FFF "C8051F533" <mcu-device> spush
  0 0x00FFF 0x00FFF "C8051F534" <mcu-device> spush
! 2K
  0 0x007FF 0x007FF "C8051F536" <mcu-device> spush
  0 0x007FF 0x007FF "C8051F537" <mcu-device> spush
! F53xA/52xA 8K
  0 0x01DFF 0x01DFF "C8051F520A" <mcu-device> spush
  0 0x01DFF 0x01DFF "C8051F521A" <mcu-device> spush
! 4K
  0 0x00FFF 0x00FFF "C8051F523A" <mcu-device> spush
  0 0x00FFF 0x00FFF "C8051F524A" <mcu-device> spush
! 2K
  0 0x007FF 0x007FF "C8051F526A" <mcu-device> spush
  0 0x007FF 0x007FF "C8051F527A" <mcu-device> spush
! 8K
  0 0x01DFF 0x01DFF "C8051F530A" <mcu-device> spush
  0 0x01DFF 0x01DFF "C8051F531A" <mcu-device> spush
! 4K
  0 0x00FFF 0x00FFF "C8051F533A" <mcu-device> spush
  0 0x00FFF 0x00FFF "C8051F534A" <mcu-device> spush
! 2K
  0 0x007FF 0x007FF "C8051F536A" <mcu-device> spush
  0 0x007FF 0x007FF "C8051F537A" <mcu-device> spush
! F54x
  0 0x03BFF 0x03BFF "C8051F540" <mcu-device> spush
  0 0x03BFF 0x03BFF "C8051F541" <mcu-device> spush
  0 0x03BFF 0x03BFF "C8051F542" <mcu-device> spush
  0 0x03BFF 0x03BFF "C8051F543" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F544" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F545" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F546" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F547" <mcu-device> spush
! F55x/56x/57x
  0 0x07BFF 0x07BFF "C8051F550" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F551" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F552" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F553" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F554" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F555" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F556" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F557" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F560" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F561" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F562" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F563" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F564" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F565" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F566" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F567" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F568" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F569" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F570" <mcu-device> spush
  0 0x07BFF 0x07BFF "C8051F571" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F572" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F573" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F574" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F575" <mcu-device> spush
! F58x/59x 128k
  3 0x1FBFF 0x1FBFF "C8051F580" <mcu-device> spush
  3 0x1FBFF 0x1FBFF "C8051F581" <mcu-device> spush
  3 0x1FBFF 0x1FBFF "C8051F582" <mcu-device> spush
  3 0x1FBFF 0x1FBFF "C8051F583" <mcu-device> spush
! 96K
  2 0x17FFF 0x17FFF "C8051F584" <mcu-device> spush
  2 0x17FFF 0x17FFF "C8051F585" <mcu-device> spush
  2 0x17FFF 0x17FFF "C8051F586" <mcu-device> spush
  2 0x17FFF 0x17FFF "C8051F587" <mcu-device> spush
! 128K
  3 0x1FBFF 0x1FBFF "C8051F588" <mcu-device> spush
  3 0x1FBFF 0x1FBFF "C8051F589" <mcu-device> spush
! 96K
  2 0x17FFF 0x17FFF "C8051F590" <mcu-device> spush
  2 0x17FFF 0x17FFF "C8051F591" <mcu-device> spush
! F7xx
  0 0x03BFF 0x03BFF "C8051F700" <mcu-device> spush
  0 0x03BFF 0x03BFF "C8051F701" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F702" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F703" <mcu-device> spush
  0 0x03BFF 0x03BFF "C8051F704" <mcu-device> spush
  0 0x03BFF 0x03BFF "C8051F705" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F706" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F707" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F708" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F709" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F710" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F711" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F712" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F713" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F714" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F715" <mcu-device> spush
  0 0x03BFF 0x03BFF "C8051F716" <mcu-device> spush
  0 0x03BFF 0x03BFF "C8051F717" <mcu-device> spush
! F80x/81x/82x/83x
  0 0x03FFF 0x03FFF "C8051F800" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F801" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F802" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F803" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F804" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F805" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F806" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F807" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F808" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F809" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F810" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051F811" <mcu-device> spush
!
  0 0x01FFF 0x01FFF "C8051F812" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F813" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F814" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F815" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F816" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F817" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F818" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F819" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F820" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F821" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F822" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F823" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F824" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F825" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F826" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F827" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F828" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F829" <mcu-device> spush
!
  0 0x00FFF 0x00FFF "C8051F830" <mcu-device> spush
  0 0x00FFF 0x00FFF "C8051F831" <mcu-device> spush
  0 0x00FFF 0x00FFF "C8051F832" <mcu-device> spush
  0 0x00FFF 0x00FFF "C8051F833" <mcu-device> spush
  0 0x00FFF 0x00FFF "C8051F834" <mcu-device> spush
  0 0x00FFF 0x00FFF "C8051F835" <mcu-device> spush
!  F90x/91x
  0 0x01FFF 0x01FFF "C8051F901" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F902" <mcu-device> spush
  0 0x03BFF 0x03BFF "C8051F911" <mcu-device> spush
  0 0x03BFF 0x03BFF "C8051F912" <mcu-device> spush
! F93x/92x 32K
  0 0x07FFF 0x07FFF "C8051F920" <mcu-device> spush
  0 0x07FFF 0x07FFF "C8051F921" <mcu-device> spush
! 64K
  0 0x0FBFF 0x0FBFF "C8051F930" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "C8051F931" <mcu-device> spush
! F99x_98x
  0 0x01FFF 0x01FFF "C8051F980" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F981" <mcu-device> spush
  0 0x00FFF 0x00FFF "C8051F982" <mcu-device> spush
  0 0x00FFF 0x00FFF "C8051F983" <mcu-device> spush
  0 0x007FF 0x007FF "C8051F985" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F986" <mcu-device> spush

  0 0x01FFF 0x01FFF "C8051F987" <mcu-device> spush
  0 0x00FFF 0x00FFF "C8051F988" <mcu-device> spush
  0 0x00FFF 0x00FFF "C8051F989" <mcu-device> spush

  0 0x01FFF 0x01FFF "C8051F990" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F991" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F996" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051F997" <mcu-device> spush
!  T32x
  0 0x03FF8 0x03FF8 "C8051T320" <mcu-device> spush
  0 0x03FF8 0x03FF8 "C8051T321" <mcu-device> spush
  0 0x03FF8 0x03FF8 "C8051T322" <mcu-device> spush
  0 0x03FF8 0x03FF8 "C8051T323" <mcu-device> spush
  0 0x03FF8 0x03FF8 "C8051T326" <mcu-device> spush
  0 0x03FF8 0x03FF8 "C8051T327" <mcu-device> spush
! T60x 8K
  0 0x01FFF 0x01FFF "C8051T600" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051T601" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051T602" <mcu-device> spush
! 4K
  0 0x01FFF 0x01FFF "C8051T603" <mcu-device> spush
! 2K
  0 0x01FFF 0x01FFF "C8051T604" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051T605" <mcu-device> spush
! 2K
  0 0x007FF 0x007FF "C8051T606" <mcu-device> spush
! T61x  16k
  0 0x03FFF 0x03FFF "C8051T610" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051T611" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051T612" <mcu-device> spush
! 8K
  0 0x03FFF 0x03FFF "C8051T613" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051T614" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051T615" <mcu-device> spush
! 16k
  0 0x03FFF 0x03FFF "C8051T616" <mcu-device> spush
  0 0x03FFF 0x03FFF "C8051T617" <mcu-device> spush
! T62x
  0 0x03FF8 0x03FF8 "C8051T620" <mcu-device> spush
  0 0x03FF8 0x03FF8 "C8051T621" <mcu-device> spush
  0 0x03FF8 0x03FF8 "C8051T622" <mcu-device> spush
  0 0x03FF8 0x03FF8 "C8051T623" <mcu-device> spush
! T63x 8K
  0 0x01FFF 0x01FFF "C8051T630" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051T631" <mcu-device> spush
! 4K
  0 0x01FFF 0x01FFF "C8051T632" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051T633" <mcu-device> spush
! 2K
  0 0x01FFF 0x01FFF "C8051T634" <mcu-device> spush
  0 0x01FFF 0x01FFF "C8051T635" <mcu-device> spush

  0 0x0FBFF 0x0FBFF "Si1000" <mcu-device> spush
  0 0x07FFF 0x07FFF "Si1001" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "Si1002" <mcu-device> spush
  0 0x07FFF 0x07FFF "Si1003" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "Si1004" <mcu-device> spush
  0 0x07FFF 0x07FFF "Si1005" <mcu-device> spush

  0 0x03BFF 0x03BFF "Si1010" <mcu-device> spush
  0 0x01FFF 0x01FFF "Si1011" <mcu-device> spush
  0 0x03BFF 0x03BFF "Si1012" <mcu-device> spush
  0 0x01FFF 0x01FFF "Si1013" <mcu-device> spush
  0 0x03BFF 0x03BFF "Si1014" <mcu-device> spush
  0 0x01FFF 0x01FFF "Si1015" <mcu-device> spush
  0 0x0FBFF 0x0FBFF "Si2501" <mcu-device> spush

;
